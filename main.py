from taalbot import BOT
import data.localdata as ld

assert ld.id_server, "id_server not defined in localdata"
assert ld.id_application, "id_application not defined in localdata"

assert ld.id_log_channel, "id_log_channel not defined in localdata"
assert ld.id_welcome_channel, "id_welcome_channel not defined in localdata"
assert ld.id_mail_channel, "id_mail_channel not defined in localdata"
assert ld.id_douane_channel, "id_douane_channel not defined in localdata"

assert ld.id_leerkracht_role, "id_leerkracht_role not defined in localdata"
assert ld.id_douanier_role, "id_douanier_role not defined in localdata"
assert ld.id_warning_role, "id_warning_role not defined in localdata"
assert ld.id_tracker_role, "id_tracker_role not defined in localdata"

assert ld.color_country_role, "color_country_role not defined in localdata"
assert ld.cogs_to_load, "cogs_to_load not defined in localdata"

# TODO: FINISH THIS CHECK

with open(".token", "r") as file: TOKEN = file.read()

BOT.run(TOKEN)
