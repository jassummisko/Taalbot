import discord
from random import choice

# General responses
def ELK_ZINNEN_DAH() -> str: 
    return "Elk zinnen dah!"
def NOT_STAFF_ERROR() -> str: 
    return "Alleen staff kan dit commando gebruiken."
def NOT_POST_OWNER_ERROR() -> str: 
    return "Dit is niet jouw post. Je kan hem niet bewerken."
def NOT_POST_OWNER_OR_STAFF_ERROR() -> str: 
    return "Je moet staff zijn om iemands post te bewerken."
def THREAD_ALREADY_ANSWERED() -> str: 
    return "Deze thread staat al op beantwoord."
def THREAD_ANSWERED() -> str: 
    return "Deze thread staat nu op beantwoord."
def TIMED_OUT() -> str: 
    return "Timed out!"

# Birthday stuff
def VERJAARDAG(member_mention: str) -> str:
    EMOJIS = [
        "🥳 ",
        "<:joepie:1201320540200255570>",
        "🎊",
        "🎂",
        "🍰",
        "🧁",
        "🎁",
        "👑",
        "🥂",
        "🎉",
        "🪩",
        "🎶",
        "<:knuffelspook:669184099713548299>",
    ]

    MESSAGES = [
        "Gefeliciteerd!",
        "Er is er eentje jarig!",
        "Hiep hiep hoera!",
        "Gelukkige verjaardag!",
        "Fijne verjaardag!",
        "Lang zal je leven!",
        "Een jaartje wijzer!",
    ]

    selected_emoji = choice(EMOJIS)
    selected_msg = choice(MESSAGES)

    return f" {selected_msg} {selected_emoji} {member_mention}"


# Role manager responses
def ROLE_GIVEN(roleName: str) -> str: 
    return f"Rol gegeven: '{roleName}'"
def ROLE_REMOVED(roleName: str) -> str: 
    return f"Rol verwijderd: '{roleName}'"

# Douane responses
def DOUANE_TIMEOUT(user: str, target: str) -> str:
    return f"🛃 {user} heeft een timeout gegeven aan {target}"
def DOUANE_UNTIMEOUT(user: str, target: str) -> str:
    return f"🛃 {user} heeft de timeout verwijderd van {target}"
def DOUANE_DELETED(user: str, message: str) -> str:
    return f"🛃 {user} heeft het volgende bericht gedeletet: \n \"{message}\"."


# Channel manager responses
def NOT_IN_FORUM_ERROR() -> str: 
    return "Je zit niet in een forum"
def NOT_IN_KELDER_ERROR() -> str: 
    return "Je zit niet in #kelder"
def KELDER_LIMIET_ERROR() -> str: 
    return "De limiet moet tussen 3 en 8 liggen."
def KELDER_LIMIER_UPDATED(limit: int) -> str: 
    return f"De limiet is nu {limit}."

# FAQ responses
def UPDATING_FAQ() -> str: 
    return f"FAQs aan het updaten..."
def FAQ_UPDATED() -> str: 
    return f"FAQs geüpdatet"
def RUNNING_FAQ(label: str) -> str: 
    return f"Running FAQ {label}"
def FAQ_ENDED() -> str: 
    return f"FAQ ended."
def FAQ_DEREGISTERED(faqName: str) -> str: 
    return f"FAQ '{faqName}' verwijderd."
def FAQ_REGISTERED(faqName: str, label: str) -> str: 
    return f"FAQ '{faqName}' is geregistreerd met label '{label}' als begin."
def NOT_FAQ_ERROR(faqName: str) -> str: 
    return f"Er zit geen FAQ met de '{faqName}' in de lijst."

# Scraper responses
def DEHET_NOWORD(woord: str) -> str: 
    return f"Ik heb geen woorden kunnen vinden voor '{woord}'"
def DEHET_NONOUN(woord: str) -> str: 
    return f"Ik heb geen substantieven kunnen vinden voor '{woord}'"
def DEHET_SINGLEWORD(lidwoord: str, woord: str) -> str: 
    return f"****{lidwoord}** {woord}**"
def DEHET_MULTIWORD(lidwoord: str, woord: str, betekenis: str) -> str: 
    return f"****{lidwoord}** {woord}** met de betekenis **{betekenis}**\n"

# Mod mail responses
def MAIL_EMBED_RECEIVED(title: str, message: str, author: str) -> discord.Embed:
    if author == "/": author = "Anonymous"
    return discord.Embed(
                title=title,
                description=message,
            ).set_footer(text=author)
def MAIL_CHOOSE_MAILTYPE() -> str: return \
"""What kind of mail are you sending?
```
1. Feedback (about the server or staff team)
2. Report (a behavior or user/s)
3. Niveau (checking your own or another user's level)
4. Sessie (ideas for sessions, or a request to host one yourself)
5. Bot (bug reports, ideas for extra functions, etc.) 
0. Other
```
Type the number as a message"""
def MAIL_ANON_NOTIF_AND_CANCEL() -> str: 
    return "You will be given the option to submit anonymously at the end. Type \"cancel\" to cancel the modmail wizard."
def MAIL_TYPE_YOUR_MESSAGE() -> str: 
    return "You may now type your message. You will be given a chance to edit the message before sending."
def MAIL_TYPE_SEND() -> str: return """
If you have no more edits to make: 
- type **send** to send the message with your name attached to it
- type **sendanon** to send the message anonymously. Keep in mind that sending anonymously will mean that we cannot respond to you and that we cannot ask you for feedback, so please make sure to make your message as clear as possible.

If you want to make any edits, simply edit the message using Discord's usual editing function.
""".strip()
def MAIL_SENT(recipient: str) -> str: return f"Your message has been sent to {recipient}."
def MAIL_REPLY_WITH_SENDMESSAGE() -> str: return "Stuur `!!!sendmessage` als antwoord op een bericht om het te versturen."
def MAIL_NO_SPAM() -> str: return "You've already sent 3 mod mails today. Please wait until tomorrow."
def MAIL_TIMED_OUT() -> str: return  "Your request timed out. Please start the mod mail process over."
def MAIL_USE_SENDMAIL(cmd: str) -> str: return f"If you want to send mod mail, first type the message **{cmd}**. You will be able to choose whether to send your mail anonymously at the end of the mail wizard.\nIf you meant to reply to a mod-mail, please reply to the embed of the message instead using Discord's native reply feature."
def MAIL_NOT_IN_THREAD() -> str: return "Je zit niet in een thread."
def MAIL_NOT_IN_FORUM() -> str: return "Je zit niet in een forumkanaal."
def MAIL_NOT_IN_MAIL_CHANNEL() -> str: return "Je zit niet in het staffmailkanaal."
def MAIL_NO_REPLY_MESSAGE() -> str: return "No reply message found."
def MAIL_NOT_APPROVED(needed_count: int) -> str: return f"The message has not been approved by enough staff members ({needed_count}). Place 📨 as a reaction to the message to approve it."
def MAIL_RESPOND() -> str: return "Als je op dit bericht wilt reageren, stuur `!!!sendmessage` als antwoord op dit bericht."
def MAIL_NO_USER_FOUND_IN_THREAD() -> str: return "Ik heb geen ontvanger kunnen vinden. Gelieve het starterbericht te bewerken zodat het met een gebruikerstag begint."
def MAIL_TYPE_TO_GO_BACK() -> str: return "Type **oeps** if you want to go back to the previous step. Type **cancel** if you want to stop the mod mail process."

def LOG_USER_JOINED_VC(user: str, vc: str) -> str:
    return f"🔈 **{user}** is {vc} binnengetreden."
def LOG_USER_LEFT_VC(user: str, vc: str) -> str:
    return f"🔇 **{user}** heeft {vc} verlaten."

# Misc
def SPY_FOUND(spy_mention: str) -> str:
    return f"Spy bot found: {spy_mention}. User has been banned."