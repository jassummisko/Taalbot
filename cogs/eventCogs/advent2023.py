import discord, utils.genUtils as genUtils, datetime, pickle
from modules.scraper.dictionary import CreateEntry
from enum import Enum
from random import choices
from cogs.scraperCog import scraperCog
from discord.ext import commands, tasks
from typing import cast
from discord import ChannelType, app_commands
from data.localdata import id_server, native_roles
from modules.logger import logger
from cogs.eventCogs.advent2023data import \
    CALENDAR, ADVENT_EMOJI, ID_ADVENT_EMOJI, ID_ADVENT_ROLE, \
    NOTIFICATION_POST, ID_ADVENT_CHANNEL, \
    ID_RULES_POST_DEFINITIES, ID_RULES_POST_VOORBEELDEN, \
    ID_RULES_POST_CHANNEL, ID_RULES_POST_PICTURE, \
    PICTURE_POST, IMAGES

class AdventError(Enum):
    SUCCESS = 0
    WORD_NOT_FOUND = -1
    INVALID_CEFR = -2
    INVALID_USER_LEVEL = -3

class advent2023(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.leaderboard_path = "cogs/eventCogs/advent2023leaderboard.bin"

        self.bot = bot
        self.log_channel: discord.TextChannel
        self.advent_channel: discord.TextChannel
        self.advent_role: discord.Role
        self.leaderboard: dict[int, int] = {}
        self.rules_post_channel: discord.TextChannel|discord.Thread

    async def cog_load(self):
        await super().cog_load()

    @commands.Cog.listener()
    async def on_ready(self):
        print("Advent 2023 Cog is ready")

        self.log_channel = logger.getLogChannel()

        advent_channel = self.bot.get_channel(ID_ADVENT_CHANNEL)
        assert isinstance(advent_channel, discord.TextChannel)
        self.advent_channel = advent_channel

        rules_post_channel = self.bot.get_channel(ID_RULES_POST_CHANNEL)
        assert isinstance(rules_post_channel, discord.TextChannel) or isinstance(rules_post_channel, discord.Thread)
        self.rules_post_channel = rules_post_channel

        self._loadLeaderboard()

        try:
            self.adventTask.start()
        except RuntimeError:
            print("Task already running.")

    @tasks.loop(time=datetime.time(hour=11,minute=00,tzinfo=datetime.timezone.utc))
    async def adventTask(self): 
        now = datetime.datetime.now()
        if now.month != 12: return

        await self._doThread()
    
    @app_commands.command(name="reloadadvent", description="Template")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def reload(self, i9n: discord.Interaction):
        await self.on_ready()
        await i9n.response.send_message(f"Advent cog reloaded. Loaded channel mention: {self.advent_channel.mention}")

    @app_commands.command(name="makepost", description="Template")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def postOfTheDay(self, i9n: discord.Interaction, day_idx: int|None):
        channel = i9n.channel
        assert isinstance(channel, discord.TextChannel)

        await self._doThread(day_idx, channel)
        await i9n.response.send_message("Done", ephemeral=True)

    @app_commands.command(name="adventrol", description="Take on the @Advent role, or remove it if you already have it")
    @genUtils.catcherrors
    async def adventrol(self, i9n: discord.Interaction):
        user = cast(discord.Member, i9n.user)

        for role in user.roles:
            if role.id == ID_ADVENT_ROLE:
                await user.remove_roles(role)
                await i9n.response.send_message("Advent role removed.")
                await self.log_channel.send(f"User {user.mention} removed his {ADVENT_EMOJI} role 😔.")
                return

        advent_role = user.guild.get_role(ID_ADVENT_ROLE)
        assert advent_role

        await user.add_roles(advent_role)
        await i9n.response.send_message("Advent role added.")
        await self.log_channel.send(f"User {user.mention} took the {ADVENT_EMOJI} role 🥳")

    @app_commands.command(name="template", description="Template")
    @genUtils.catcherrors
    async def template(self, i9n: discord.Interaction):
        pass

    @app_commands.command(name="adventsummary", description="(STAFF ONLY) Post advent summary")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def adventsummary(self, i9n: discord.Interaction, thread: discord.Thread|None, fake: bool = False):
        if not isinstance(i9n.channel, discord.Thread): return await i9n.response.send_message("Not in any thread.", ephemeral=True)
        channel = i9n.channel
        if thread: channel = thread
        await i9n.response.defer()
        await self._postSummary(channel, i9n.channel, fake)
        await i9n.followup.send("Done", ephemeral=True)

    @app_commands.command(name="advent", description="Get three random words")
    @genUtils.catcherrors
    async def advent(self, i9n: discord.Interaction, niveau: str|None):
        scraper_cog = self.bot.get_cog("scraperCog")
        if scraper_cog == None: 
            await i9n.response.send_message("Scraper Cog not loaded", ephemeral=True)
            return
        assert isinstance(scraper_cog, scraperCog)

        is_native = False

        for role in cast(discord.Member, i9n.user).roles:
            if role.id in native_roles:
                is_native = True
                break

        dictionary = scraper_cog.dict
        if is_native: dictionary = [w for w in dictionary if w[6] == ""]
        if niveau:
            words_of_level = [w for w in dictionary if str(w[5]).removesuffix("*") == niveau.upper()]
        else:
            words_of_level = dictionary

        time_minus_noon = datetime.datetime.now() - datetime.timedelta(hours=12)

        letters_of_the_day = CALENDAR[time_minus_noon.day-1]
        letters_of_the_day = letters_of_the_day.split("+")
        final_words = []
        for letter in letters_of_the_day:
            letter = letter.lower()
            final_words += [w for w in words_of_level if w[1].lower().startswith(letter)]

        if len(final_words) > 3:
            words = choices(final_words, k=3)
        else:
            await i9n.response.send_message(f"No words found of level {niveau} for today's letter.", ephemeral=True)
            return

        entries = [CreateEntry(word) for word in words]

        buffer = "\n".join([f"{e.voorvoegsel} __{e.lemma}__ ({e.erk}) - {e.definitie}" for e in entries])
        buffer = buffer.replace("/ ", "")

        await i9n.response.send_message(buffer)

    @app_commands.command(name="geefcadeaus", description="Shows your advent points earned.")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def givepoints(self, i9n: discord.Interaction, member: discord.Member, cadeaus: int):
        try: self._addPresents(member.id, cadeaus)
        except: await i9n.response.send_message("Failed", ephemeral=True)
        else: await i9n.response.send_message(f"Je hebt {cadeaus}{ADVENT_EMOJI} gegeven aan {member.mention}", ephemeral=True)

    @app_commands.command(name="mijnadvent", description="Shows your advent points earned.")
    @genUtils.catcherrors
    async def mijnadvent(self, i9n: discord.Interaction):
        tree = self._makeTree(i9n.user.id)
        await i9n.response.send_message(tree)

    @app_commands.command(name="checkadvent", description="Check a user's advent.")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def checkadvent(self, i9n: discord.Interaction, user: discord.Member):
        tree = self._makeTree(user.id)
        await i9n.response.send_message(tree)

    @app_commands.command(name="topadvent", description="Shows the leaderboard.")
    @genUtils.catcherrors
    async def leaderboardcmd(self, i9n: discord.Interaction):
        leaderboard_string = self._getLeaderboardString(i9n.user.id)
        await i9n.response.send_message(leaderboard_string)
    
    async def _postSummary(self, thread: discord.Thread, post_thread: discord.Thread, fake: bool = False):
        def _formatApprovedMessage(message: discord.Message, score: int) -> str:
            lines = message.content.splitlines()
            word = lines[0].replace("*", "").replace("_", "")
            sentence = lines[1]
            note = ""
            if score == 0:
                note = " (TO BE CHECKED MANUALLY)"
            return f"{message.author.mention} - {word} - {sentence} - {score} {ADVENT_EMOJI}{note}"
        
        def _calculatePoints(user: discord.Member, word: str) -> tuple[int, str]:
            scraper_cog = self.bot.get_cog("scraperCog")
            assert scraper_cog, "Scraper Cog not loaded"
            assert isinstance(scraper_cog, scraperCog)

            level_lookup = {
                "A0": 0,
                "A1": 1,
                "A2": 2,
                "B1": 3,
                "B2": 4,
                "C+": 5,
            }

            user_niveau: str = ""

            for role in user.roles:
                if role.name == "Native": return 2, ""
                if role.name.startswith("Niveau"):
                    user_niveau = role.name[-2:]
                    break

            dictionary = scraper_cog.dict

            entry: list|None = None

            for e in dictionary:
                if e[1].lower() == word.lower(): 
                    entry = e
                    break

            if not entry: return -1, f"Word '{word}' not found."

            erk = entry[5].removesuffix("*")
            if not erk: erk = "NO_LEVEL"
            if erk[0] == "C": erk = "C+"

            val_word = level_lookup.get(erk)
            val_user = level_lookup.get(user_niveau)

            if not val_word: return -1, f"Word '{word}' has no valid CEFR level."
            if not val_user: return -1, "Error in user level evaluation."

            if val_user > val_word: return 1, ""
            else: return 2, ""

        approved_messages = []

        user_point_dict = {}
        async for message in thread.history(limit=5000):
            member = message.author
            try:
                word = message.content \
                    .split("\n")[0] \
                    .replace("*", "") \
                    .replace("_", "") \
                    .removeprefix("de ") \
                    .removeprefix("het ") \
                    .removeprefix("zich ") \
                    .strip()
            except Exception as e:
                print(f"Skipped message due to error: {e}\n{message.content}")
                continue
            assert isinstance(member, discord.Member)
            for reaction in message.reactions:
                if isinstance(reaction.emoji, str): continue
                if reaction.emoji.id == ID_ADVENT_EMOJI:
                    points_earned, err = _calculatePoints(member, word)

                    if points_earned == -1:
                        print(err)
                        points_earned = 0

                    if not(member.id in user_point_dict): user_point_dict[member.id] = 0
                    user_point_dict[member.id] += points_earned

                    approved_messages.append(_formatApprovedMessage(message, points_earned))

        if not fake:
            for user_id, points in user_point_dict.items():
                print(f"{user_id} -- {points}")
                self._addPresents(user_id, min(points, 10))

        approved_messages = sorted(approved_messages, key=lambda x: x.split(" - ")[1].lower().removeprefix("het ").removeprefix("de ").removeprefix("zich ")) #type: ignore

        buffer = "## Approved posts of the day\n"
        final_messages = []
        for message in approved_messages:
            if len(buffer + message) + 1 > 2000:
                final_messages.append(buffer)
                buffer = ""
            else:
                buffer += message + "\n"

        final_messages.append(buffer)

        for m in final_messages: await post_thread.send(m)

    def _getLetterPost(self, letter: str) -> str: 
        if len(letter) == 1: return f"# De letter van vandaag is: {letter}"
        return f"# De letters van vandaag zijn: {letter.replace('+', ' & ')}"

    def _getMostRecentAdventThreads(self) -> tuple[discord.Thread|None, discord.Thread|None]:
        threads = self.advent_channel.threads
        sorted_threads = sorted(threads, key=lambda x: x.created_at, reverse=True) #type: ignore

        t1 = None
        try: t1 = sorted_threads[0]
        except: pass

        t2 = None
        try: t2 = sorted_threads[1]
        except: pass

        return t1, t2

    async def _doThread(self, day_idx: int|None = None, channel: discord.TextChannel|None = None):
        closing_message = "De thread is gesloten. Dank jullie wel! <@&259993681950408704>"
        if not channel: channel = self.advent_channel
        last_threads = self._getMostRecentAdventThreads()
        last_thread_1, last_thread_2 = last_threads
        if last_thread_1 and not last_thread_1.locked: 
            await last_thread_1.send(closing_message)
            await last_thread_1.edit(locked=True)
        if last_thread_2 and not last_thread_2.locked: 
            await last_thread_2.send(closing_message)
            await last_thread_2.edit(locked=True)

        if day_idx == None: day_idx = datetime.datetime.today().day - 1

        try: 
            letter_of_the_day = CALENDAR[day_idx]
        except IndexError as e: 
            letter_of_the_day = "?"

        if letter_of_the_day == "?q": 
            await self._doPictureThread(day_idx, channel)
        else:
            await self._doLetterThread(day_idx, letter_of_the_day, channel)

    async def _doPictureThread(self, day_idx: int, channel: discord.TextChannel):
        await channel.send(PICTURE_POST())
        picture_rules = await self.rules_post_channel.fetch_message(ID_RULES_POST_PICTURE)
        picture_thread_of_the_day = await channel.create_thread(name=f"Speciaal - 9 dec", type=ChannelType.public_thread)
        await picture_thread_of_the_day.send(f"\n{picture_rules.content}")
        await picture_thread_of_the_day.send(IMAGES[day_idx])

    async def _doLetterThread(self, day_idx: int, letter_of_the_day: str, channel: discord.TextChannel):
        starter_message = self._getLetterPost(letter_of_the_day)

        await channel.send(NOTIFICATION_POST(letter_of_the_day))
        definities_rules = await self.rules_post_channel.fetch_message(ID_RULES_POST_DEFINITIES)
        definition_thread_of_the_day = await channel.create_thread(name=f"Definities - {day_idx+1} dec - {letter_of_the_day}", type=ChannelType.public_thread)
        await definition_thread_of_the_day.send(starter_message
            + f"\n{definities_rules.content}"
        )

        voorbeelden_rules = await self.rules_post_channel.fetch_message(ID_RULES_POST_VOORBEELDEN)
        example_thread_of_the_day = await channel.create_thread(name=f"Voorbeelden - {day_idx+1} dec - {letter_of_the_day}", type=ChannelType.public_thread)
        await example_thread_of_the_day.send(
            starter_message
            + f"\n{voorbeelden_rules.content}"
        )

    def _getLeaderboardString(self, caller_id: int) -> str:
        ordered_leaderboard = list(self.leaderboard.items())
        ordered_leaderboard = sorted(ordered_leaderboard, key=lambda x: x[1], reverse=True)

        if len(ordered_leaderboard) < 3: return "Er zijn voorlopig minder dan 3 adventers."

        caller_position: int = 0
        caller_points: int = 0

        for i, entry in enumerate(ordered_leaderboard):
            if caller_id == entry[0]:
                caller_position = i+1
                caller_points = entry[1]

        user_first  = self.bot.get_user(ordered_leaderboard[0][0])
        user_second = self.bot.get_user(ordered_leaderboard[1][0])
        user_third  = self.bot.get_user(ordered_leaderboard[2][0])

        assert user_first  
        assert user_second 
        assert user_third  

        return f"""**Fervente {self.advent_channel.mention}ers**
🥇 {user_first.display_name} heeft {ordered_leaderboard[0][1]} {ADVENT_EMOJI} 
🥈 {user_second.display_name} heeft {ordered_leaderboard[1][1]} {ADVENT_EMOJI}
🥉 {user_third.display_name} heeft {ordered_leaderboard[2][1]} {ADVENT_EMOJI} 
🎄🎄🎄🎄🎄 
{ADVENT_EMOJI} En jij bent #{caller_position} met {caller_points} {ADVENT_EMOJI} 
"""

    def _addPresents(self, user_id: int, amount_of_presents: int):
        if self.leaderboard.get(user_id) == None: self.leaderboard[user_id] = 0
        self.leaderboard[user_id] += amount_of_presents
        self._saveLeaderboard()

    def _makeTree(self, user_id: int) -> str:
        user = self.advent_channel.guild.get_member(user_id)
        assert isinstance(user, discord.Member)

        has_advent = False
        for role in user.roles:
            if role.id == ID_ADVENT_ROLE: has_advent = True
        
        if not has_advent: return f"Sorry, jouw kerstboom is nog niet versierd! 🌲 Zo kan je er geen {ADVENT_EMOJI} onder leggen... Schrijf `/adventrol` om mee te doen."

        amount_of_presents = self.leaderboard.get(user_id)
        if amount_of_presents == None: amount_of_presents = 0
        return f"""{user.mention} Jij hebt {amount_of_presents} 🎁 verdiend in {self.advent_channel.mention}!```
❄    ⭐     ❄     ❆ 
    .o.'.               ❅     
   .'.'o'.      ❄     
  o'.o.'.o.            
 .'.o.'.'.o.            ❄                   
.o.'.o.'.o.'.       ❅       
     | |      """.strip() + ("🎁" * amount_of_presents) + "\n```"

    def _saveLeaderboard(self):
        with open(self.leaderboard_path, "wb") as f:
            pickle.dump(self.leaderboard, f)

    def _loadLeaderboard(self):
        try:
            f = open(self.leaderboard_path, "rb")
        except FileNotFoundError:
            self.leaderboard = {}
        else:
            with f:
                self.leaderboard = pickle.load(f)
                f.close()

async def setup(bot):
    await bot.add_cog(advent2023(bot), guilds = [discord.Object(id = id_server)])