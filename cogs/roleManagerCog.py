import discord, typing, \
    data.botResponses as botResponses, \
    utils.genUtils as genUtils
from data.localdata import \
    id_server, id_leerkracht_role, color_country_role, pronoun_roles, province_roles, id_sessiehost_channel
from discord import Interaction, Member, app_commands
from discord.ext import commands
from modules.roleManager.roleManager import *
from discord.app_commands import Choice
from fuzzywuzzy import fuzz
from discord.ext.commands import CommandError
from modules.logger import logger

class roleManagerCog(commands.Cog):
    def __init__(self, bot):
        self.bot: discord.Client = bot
        self.niveauslot_dict = {}
        if not self.niveauslot_dict:
            try: 
                with open(level_fate_path, "r") as f: 
                    self.niveauslot_dict = json.load(f)
            except FileNotFoundError as e:
                self.niveauslot_dict = {}

    @commands.Cog.listener()
    async def on_ready(self):
        print("Role Manager Cog is ready")

    @commands.Cog.listener()
    async def on_member_update(self, member_before: discord.Member, member_after: discord.Member):
        # If role taken
        if len(member_before.roles) < len(member_after.roles):
            new_role = next(
                role 
                for role in member_after.roles 
                if role not in member_before.roles
            )

            # Check niveau
            if new_role.name.lower().startswith("niveau"):
                # Remove and change back to fate role
                target_niveau_id = self.niveauslot_dict.get(str(member_before.id))
                if not target_niveau_id: return

                guild = self.bot.get_guild(id_server)
                target_niveau = guild.get_role(target_niveau_id)
                if new_role == target_niveau: return

                await member_after.remove_roles(new_role)
                assert(target_niveau)
                await member_after.add_roles(target_niveau)
                await logger.log(f"📊 User tried to escape their fate unsuccessfully. Their fated role is {target_niveau.mention}.") 

    @app_commands.command(name="giveleerkrachtrole", description="Geef de leerkracht rol aan iemand")
    @app_commands.describe(user="username", duration="duration in minutes")
    @genUtils.catcherrors
    async def giveleerkrachtrole(self, i9n: Interaction, user: Member, duration: int = 180) -> None:
        assert i9n.guild
        assert isinstance(callingUser := i9n.user, discord.Member)
        assert (leerkrachtRole := i9n.guild.get_role(id_leerkracht_role))

        if not genUtils.isStaff(callingUser): raise CommandError(botResponses.NOT_STAFF_ERROR()) 

        await giveTemporaryRole(user, leerkrachtRole, duration)
        await i9n.response.send_message(botResponses.ROLE_GIVEN(leerkrachtRole.mention))

    @app_commands.command(name="krijgleerkrachtrol", description="Geef de leerkracht rol aan iemand")
    @app_commands.describe(duration="duration in minutes")
    @genUtils.catcherrors
    async def krijgleerkrachtrol(self, i9n: Interaction, duration: int = 180) -> None:
        #TODO: Move to botResponses
        if not i9n.guild: return await i9n.response.send_message("Je zit niet op Nederlands Leren.")
        assert i9n.channel
        assert isinstance(callingUser := i9n.user, discord.Member)
        assert (leerkrachtRole := i9n.guild.get_role(id_leerkracht_role))

        if i9n.channel.id != id_sessiehost_channel:
            #TODO: Move to botResponses
            return await i9n.response.send_message("Je zit niet in het sessiehost-kanaal", ephemeral=True)

        await giveTemporaryRole(callingUser, leerkrachtRole, duration)
        await i9n.response.send_message(botResponses.ROLE_GIVEN(leerkrachtRole.mention))

    async def provincierol_autocomplete(self, i9n: discord.Interaction, current: str):
        assert i9n.guild

        all_roles = [typing.cast(discord.Role, i9n.guild.get_role(id)) for id in province_roles]

        current = current.lower()
        matched_roles = [role for role in all_roles if role.name.lower().startswith(current)]

        roles = sorted(
            matched_roles,
            reverse=True
        )
        if len(roles) > 10:
            return [Choice(name=role.name, value=role.name) for role in roles[:10]] 
        else:
            return [Choice(name=role.name, value=role.name) for role in roles] 

    @app_commands.command(name="provincierol", description="Assign province roles")
    @app_commands.describe(provincie="De naam van het land")
    @app_commands.autocomplete(provincie=provincierol_autocomplete)
    @genUtils.catcherrors
    async def provincierol(self, i9n: discord.Interaction, provincie: str):
        assert i9n.guild
        assert isinstance(calling_user := i9n.user, discord.Member)

        all_roles = [typing.cast(discord.Role, i9n.guild.get_role(id)) for id in province_roles]

        province_role: discord.Role|None = None
        for role in all_roles:
            if provincie.lower() == role.name.lower():
                province_role = role

        if not province_role: return await i9n.response.send_message("Je hebt geen geldige provincienaam ingetypt.")

        await calling_user.add_roles(province_role)
        await i9n.response.send_message(f"Alsjeblieft! Je hebt nu de rol {province_role.name}")

    async def landrol_autocomplete(self, i9n: discord.Interaction, current: str):
        assert i9n.guild
        roles = sorted(
            [role.name for role in i9n.guild.roles if role.color == color_country_role], 
            key=(lambda role: fuzz.ratio(role.lower(), current.lower())), 
            reverse=True
        )
        return [Choice(name=role, value=role) for role in roles[:10]] 

    @app_commands.command(name="landrol", description="Assign country roles")
    @app_commands.describe(land="De naam van het land")
    @app_commands.autocomplete(land=landrol_autocomplete)
    @genUtils.catcherrors
    async def landrol(self, i9n: discord.Interaction, land: str):
        assert i9n.guild
        assert isinstance(calling_user := i9n.user, discord.Member)

        responseMsg = "Alsjeblieft!"
        await calling_user.add_roles([role for role in i9n.guild.roles if role.name == land][0])
        await i9n.response.send_message(responseMsg)

    @app_commands.command(name="niveaurol", description="Assign level roles")
    @app_commands.describe(niveau="CEFR niveau")
    @genUtils.catcherrors
    async def niveaurol(self, i9n: discord.Interaction, niveau: str = ""):
        assert i9n.guild
        assert isinstance(i9n.user, discord.Member)

        if self.niveauslot_dict.get(str(i9n.user.id)):
            await i9n.response.send_message("Nee!", ephemeral=True)
            return

        niveau_roles = [role for role in i9n.guild.roles if ("Niveau" in role.name) and not ("C+" in role.name)]
        niveau_role_names = [role.name for role in niveau_roles]
        # TODO: Make this less ugly.
        if niveau in niveau_role_names:
            oldrole = [role for role in i9n.user.roles if "Niveau" in role.name][0]
            await i9n.user.remove_roles(oldrole)
            roles_to_add = [role for role in niveau_roles if role.name == niveau]
            assert (
                member := i9n.guild.get_member(i9n.user.id)
            )
            await member.add_roles(*roles_to_add)
            # TODO: Move these to botResponses
            await i9n.response.send_message(
                f"Changed role {oldrole.name} to {[role.name for role in roles_to_add][0]} for user {i9n.user.mention}"
            )
            await logger.log(f"📊 User {i9n.user.mention} changed their role from {oldrole.name} to {[role.name for role in roles_to_add][0]}") 
        else:
            view = await niveauRolSelectionView(i9n.guild)
            await i9n.response.send_message("Here you go!", view=view)

    @niveaurol.autocomplete('niveau')
    async def niveaurol_autocomplete(self, i9n: discord.Interaction, current: str):
        assert i9n.guild
        niveauRoles = [role for role in i9n.guild.roles if ("Niveau" in role.name) and not ("C+" in role.name)]
        niveauRoleNames = [role.name for role in niveauRoles]
        return [Choice(name=role, value=role) for role in niveauRoleNames] 

    @app_commands.command(name="niveauslot", description="(STAFF) Niveauslot bepalen")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def niveauslot(self, i9n: discord.Interaction, lid: discord.Member, niveau: str):

        def save_lot():
            with open(level_fate_path, "w") as f: 
                json.dump(self.niveauslot_dict, f)

        if niveau == "geen":
            if self.niveauslot_dict.get(str(lid.id)): 
                del self.niveauslot_dict[str(lid.id)]
                await i9n.response.send_message(f"{lid.mention} heeft geen niveauslot meer.", ephemeral=True)
                save_lot()
            else:
                await i9n.response.send_message(f"{lid.mention} heeft geen niveauslot.", ephemeral=True)
            return

        niveau_role: discord.Role|None
        niveau_roles = [role for role in i9n.guild.roles if ("Niveau" in role.name)]
        for role in niveau_roles:
            if niveau == role.name:
                niveau_role = role

        if niveau_role == None:
            #TODO: Put in botResponses
            await i9n.response.send_message("Niveaurol niet gevonden.", ephemeral=True)
            return

        self.niveauslot_dict[str(lid.id)] = niveau_role.id
        save_lot()

        for role in lid.roles:
            if role.name.lower().startswith("niveau"):
                await lid.remove_roles(role)
        await lid.add_roles(niveau_role)

        await i9n.response.send_message(f"Niveauslot veranderd van {lid.mention}: {niveau_role.mention}.")

    @niveauslot.autocomplete('niveau')
    async def niveauslot_autocomplete(self, i9n: discord.Interaction, current: str):
        choices = await self.niveaurol_autocomplete(i9n, current)
        choices.append(Choice(name="Niveau C+", value="Niveau C+"))
        choices.append(Choice(name="Geen lot", value="geen"))
        return choices

    @app_commands.command(name="voornaamwoordrol", description="Assign pronoun roles")
    @app_commands.describe(vnw="The pronoun")
    @genUtils.catcherrors
    async def voornaamwoordrol(self, i9n: discord.Interaction, vnw: str):
        assert i9n.guild
        assert isinstance(calling_user := i9n.user, discord.Member)

        response_msg = "Alsjeblieft!"
        await calling_user.add_roles([role for role in i9n.guild.roles if role.name == vnw][0])
        await i9n.response.send_message(response_msg)

    @voornaamwoordrol.autocomplete('vnw') # type: ignore
    @genUtils.catcherrors
    async def voornaamwoordrol_autocomplete(self, i9n: discord.Interaction, _: str):
        roles: list
        assert i9n.guild
        roles = [i9n.guild.get_role(role_id) for role_id in pronoun_roles] 
        return [Choice(name=role.name, value=role.name) for role in roles] 

async def setup(bot):
    await bot.add_cog(roleManagerCog(bot), guilds=[discord.Object(id=id_server)])