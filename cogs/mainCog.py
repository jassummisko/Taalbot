import discord, \
    data.botResponses as botResponses, \
    utils.genUtils as genUtils, typing, datetime
from discord.ext import commands, tasks
from discord import app_commands
from data.localdata import id_server
from modules.beginners.beginners import *
from modules.logger import logger
from data.localdata import id_douane_channel, id_welles_nietes_channel
from modules.verjaardag import verjaardag

class mainCog(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.log_channel: discord.TextChannel
        self.welcome_channel: discord.TextChannel
        self.verjaardagskalender: verjaardag.BdayCalendar

    @commands.Cog.listener()
    async def on_ready(self):
        print("Logger Cog is ready")

        self.log_channel = logger.getLogChannel()
        self.welcome_channel = logger.getWelcomeChannel()
        self.douane_channel = typing.cast(discord.TextChannel, self.bot.get_channel(id_douane_channel))

        try: self.verjaardagskalender = verjaardag.loadCal()
        except: self.verjaardagskalender = {}

        await self.log_channel.send(f"Bleep bloop. I am here, ready to serve requests!")
        await self.bot.change_presence(activity=discord.Game(name="DM = staff mail"))

        try:
            self.checkEnPostVerjaardag.start()
        except RuntimeError:
            print("Task already running.")

    @commands.Cog.listener()
    async def on_message(self, msg: discord.Message):
        if isinstance(msg.channel, discord.Thread) and msg.channel.id == id_welles_nietes_channel:
            should_skip = False
            last_message: discord.Message
            async for message in msg.channel.history(limit=2):
                if should_skip:
                    last_message = message
                    break
                should_skip = True

            if (last_message.content == "WELLES" and msg.content != "NIETES") or \
                (last_message.content == "NIETES" and msg.content != "WELLES"):
                await msg.delete()

    @tasks.loop(time=datetime.time(hour=11,minute=0,tzinfo=datetime.timezone.utc))
    async def checkEnPostVerjaardag(self):
        print("Posting birthdays")
        guild = self.bot.get_guild(id_server)
        assert guild
        user_ids = verjaardag.getBirthdayIdsToday(self.verjaardagskalender)
        found_users: list[discord.Member] = []
        for user_id in user_ids:
            member = guild.get_member(user_id)
            if not member:
                self.verjaardagskalender.pop(user_id, None)
                verjaardag.saveCal(self.verjaardagskalender)
            else:
                found_users.append(member)

        for user in found_users:
            await self.welcome_channel.send(botResponses.VERJAARDAG(user.mention))

    @commands.Cog.listener()
    @genUtils.catcherrors
    async def on_member_join(self, member: discord.Member):
        await self.welcome_channel.send(f"Hallo, {member.mention}. Welkom op **Nederlands Leren**!")
        await self.log_channel.send(f"📈 **{member.mention}** has joined.")

    @commands.Cog.listener()
    @genUtils.catcherrors
    async def on_voice_state_update(self, member: discord.Member, m_before: discord.VoiceState, m_after: discord.VoiceState):
        assert m_before
        assert m_after
        await logger.logVcActivity(member, m_before, m_after)

    @commands.Cog.listener()
    @genUtils.catcherrors
    async def on_member_remove(self, member: discord.Member):
        await self.log_channel.send(f"📉 **{member.name}#{member.discriminator}** has left.")

    @commands.command(description="Says hi to the bot!")
    async def hi(self, ctx: commands.Context):
        await ctx.send(botResponses.ELK_ZINNEN_DAH())

    @app_commands.command(name="hoelaat", description="Hoe laat is het?")
    @genUtils.catcherrors
    async def hoelaat(self, i9n: discord.Interaction):
        await i9n.response.send_message(content=getCurrentTimeInDutch())

    async def maand_autocomplete(self, i9n: discord.Interaction, current: str):
        assert i9n.guild

        maanden = [
            ["januari", 1],
            ["februari", 2],
            ["maart", 3],
            ["april", 4],
            ["mei", 5],
            ["juni", 6],
            ["juli", 7],
            ["augustus", 8],
            ["september", 9],
            ["oktober", 10],
            ["november", 11],
            ["december", 12],
        ]

        matched_months = sorted(
            [maand for maand in maanden if current in maand[0]], 
            reverse=True
        )
        return [
            discord.app_commands.Choice(name=maand[0], value=maand[1]) 
            for maand in matched_months[:10]
        ] 

    @app_commands.command(name="jarigop", description="Wanneer ben je jarig?")
    @app_commands.autocomplete(maand=maand_autocomplete)
    @genUtils.catcherrors
    async def jarigop(self, i9n: discord.Interaction, dag: int, maand: int):
        if not verjaardag.isValidDate(dag, maand): 
            await i9n.response.send_message("Dat is geen geldige datum.", ephemeral=True)
            return

        entry = f"{dag}/{maand}" 
        usr_id = i9n.user.id

        self.verjaardagskalender.pop(usr_id, None)
        self.verjaardagskalender[usr_id] = entry
        await i9n.response.send_message(f"Je verjaardag is geregistreerd als \"{entry}\"")
        verjaardag.saveCal(self.verjaardagskalender)

    @app_commands.command(name="verjaardagvan", description="Wanneer is ... jarig?")
    @genUtils.catcherrors
    async def verjaardagvan(self, i9n: discord.Interaction, lid: discord.Member, geheim: bool = False):
        entry = self.verjaardagskalender.get(lid.id)
        if not entry:
            await i9n.response.send_message(f"{lid.mention} heeft zich nog niet geregistreerd in onze verjaardagskalender.", ephemeral=True)
            return

        await i9n.response.send_message(f"De verjaardag van {lid.mention} valt op {entry}.", ephemeral=geheim)

    @app_commands.command(name="feliciteer", description="(STAFF) Feliciteer alle mensen die vandaag jarig zijn")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def feliciteer(self, i9n: discord.Interaction):
        await self.checkEnPostVerjaardag()
        await i9n.response.send_message("Done.", ephemeral=True)

async def setup(bot):
    await bot.add_cog(mainCog(bot), guilds = [discord.Object(id = id_server)])
