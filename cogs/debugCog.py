import discord, \
    data.botResponses as botResponses, \
    utils.genUtils as genUtils
from datetime import datetime
from discord.ext import commands
from data.localdata import id_server, province_roles_dict
from discord.ext.commands import CommandError
from random import randint
from data.spy import ALL_SPY_IDS

class debugCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_load(self) -> None:
        await super().cog_load()
        self.random_number = randint(0, 10)

    @commands.Cog.listener()
    async def on_ready(self) -> None:
        print("Debug Cog is ready")

    @commands.command()
    async def reloadcog(self, ctx: commands.Context, cog: str):
        assert isinstance(ctx.author, discord.Member)
        if not genUtils.isStaff(ctx.author): raise CommandError(botResponses.NOT_STAFF_ERROR())
        await ctx.send(f"Reloading cog {cog}")
        try:
            await self.bot.reload_extension(f"cogs.{cog}")
            await ctx.send(f"Cog {cog} reloaded.")
        except Exception as e: await ctx.send(e) # type: ignore

    @commands.command()
    async def loadcog(self, ctx: commands.Context, cog: str):
        assert isinstance(ctx.author, discord.Member)
        if not genUtils.isStaff(ctx.author): raise CommandError(botResponses.NOT_STAFF_ERROR())
        await ctx.send(f"Loading cog {cog}")
        try:
            await self.bot.load_extension(f"cogs.{cog}")
            await ctx.send(f"Cog {cog} loaded.")
        except Exception as e: await ctx.send(e) # type: ignore

    @commands.command()
    async def unloadcog(self, ctx: commands.Context, cog: str):
        assert isinstance(ctx.author, discord.Member)
        if not genUtils.isStaff(ctx.author): raise CommandError(botResponses.NOT_STAFF_ERROR())
        await ctx.send(f"Unloading cog {cog}")
        try:
            await self.bot.unload_extension(f"cogs.{cog}")
            await ctx.send(f"Cog {cog} unloaded.")
        except Exception as e: await ctx.send(e) # type: ignore

    @commands.command()
    async def getmsgs(self, ctx: commands.Context, usr_id: int):
        guild = ctx.guild
        assert guild
        msgs = []

        today = datetime.now()
        one_month_ago = today.replace(month=today.month - 1)

        for channel in guild.channels:
            if not isinstance(channel, discord.TextChannel): continue
            if channel.category_id == 447400882779324446: continue
            print(f"Scanning {channel.name}...")
            async for msg in channel.history(limit=10000, after=one_month_ago):
                if msg.author.id == usr_id:
                    msgs.append(msg.content)

        print("Saving...")
        with open("allmsgs.txt", "w") as file:
            file.write("\n".join(msgs))

        print("Dumped to file.")

    @commands.command()
    async def resynctree(self, ctx: commands.Context):
        assert isinstance(ctx.author, discord.Member)
        if not genUtils.isStaff(ctx.author): raise CommandError(botResponses.NOT_STAFF_ERROR())
        await ctx.send(f"Resyncing tree")
        try:
            await self.bot.tree.sync(guild = discord.Object(id = id_server))
            await ctx.send(f"Tree resynced.")
        except Exception as e: await ctx.send(e) # type: ignore

    @commands.command()
    async def print_tags(self, ctx: commands.Context):
        assert isinstance(ctx.channel, discord.Thread)
        assert isinstance(ctx.channel.parent, discord.ForumChannel)

        forum = ctx.channel.parent

        buffer = ""
        for tag in forum.available_tags:
            buffer += f"{tag.emoji} - {tag.id}\n"

        await ctx.channel.send(buffer.strip())

    @commands.command()
    @genUtils.catcherrors
    async def debugtest(self, _: commands.Context):
        raise Exception("Test")

    @commands.command()
    @genUtils.catcherrors
    async def scan_for_spies(self, ctx: commands.Context):
        print("Performing scan...")
        assert(guild := ctx.guild)
        for idx, member in enumerate(guild.members):
            if idx % 100 == 0:
                print(idx)
            if str(member.id) in ALL_SPY_IDS:
                print(f"{member.name} - {member.id}")

        print("Done scanning.")

async def setup(bot):
    await bot.add_cog(debugCog(bot), guilds = [discord.Object(id = id_server)])