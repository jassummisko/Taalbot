import re, random
import discord, \
    data.botResponses as botResponses, \
    utils.genUtils as genUtils
from modules.scraper.dictionary import CreateEntry, DictEntry, GetEmbed 
from discord.ext import commands
from discord import app_commands
from data.localdata import id_server
from modules.scraper.scraper import *
from modules.scraper.woordenlijst import *
from discord.ext import menus # type: ignore
import pylightxl # type: ignore

ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

UNFOUND_WORDS_FILE = "unfound_words_list.txt"

def get_clean_entries(entry: str) -> list[str]:
    SYMBOLS_TO_REMOVE = "*/"
    for s in SYMBOLS_TO_REMOVE:
        entry = entry.replace(s, "")
    return re.split("; |, ", entry.strip())

class scraperCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.loaded = False
        self.dict: list[DictEntry] = []
    
    async def readdict(self):
        self.dict = []
        self.loaded = False
        print("Loading dictionary in memory...")
        try:
            self.file = pylightxl.readxl("modules/scraper/wnt2.xlsx")
        except Exception as e:
            print("Loading failed. Reading:")
            print(e)
        else:
            print("File loading successful.")
            print("Separating all entries...")

            for letter in ALPHABET:
                sheet = self.file.ws(letter)
                for entry in sheet.rows: 
                    if "Lemma" in entry: continue
                    self.dict.append(CreateEntry(entry))

            self.loaded = True

            print("Done.")

    @commands.Cog.listener()
    async def on_ready(self):
        await self.readdict()

        print("Scraper Cog is ready")

    @app_commands.command(name="b1", description="Checks if a word a word is B1 using ishetb1.nl")
    @app_commands.describe(woord="Word to check.")
    @genUtils.catcherrors
    async def b1(self, i9n: discord.Interaction, woord: str):
        message = ScrapeB1(woord)
        await i9n.response.send_message(message)

    @app_commands.command(name="dehet", description="Is it de or het?")
    @app_commands.describe(woord="Word to check.")
    @genUtils.catcherrors
    async def dehet(self, i9n: discord.Interaction, woord: str):
        scraped: list[WordEntry] | None = checkWoordenlijst(woord)
        words: list[WordEntry] = []
        embed = discord.Embed(title=f"Resultaten voor \"{woord}\"")
        
        if not scraped: message = botResponses.DEHET_NOWORD(woord)
        else: words: list[WordEntry] = [word for word in scraped if word.partOfSpeech == "znw"]
        
        if (not words) or len(words) == 0: message = botResponses.DEHET_NONOUN(woord)
        elif len(words) == 1:
            message = botResponses.DEHET_SINGLEWORD(words[0].grammaticalInfo['art'], words[0].lemma)
        elif len(words) > 1:
            message = ""
            for word in words:
                message += botResponses.DEHET_MULTIWORD(
                    word.grammaticalInfo['art'],
                    word.lemma,
                    word.grammaticalInfo['gloss'],
                )
        else: message = "ERROR"

        embed.description = message.strip()
        await i9n.response.send_message(embed = embed)

    @app_commands.command(name="randomwoord", description="Geeft een willekeurig woord.")
    @app_commands.describe(
        niveau="Van welk niveau moet het woord zijn?", 
        begint_met="Met welke letters moet het woord beginnen?", 
        gedefinieerd="Moet het woord een definitie hebben?", 
        met_voorbeeld = "Moet het woord een voorbeeldzin hebben?",
        heeft = "Heeft het woord een bepaalde reeks letters?",
        regex = "Heeft het woord een bepaalde regex-vorm?",
        met_embed = "Wil je een embed met alle informatie van het woordenboek of wil je alleen het woord?"
    )
    @genUtils.catcherrors
    async def randomwoord(self, 
        i9n: discord.Interaction, 
        regex: str|None,
        heeft: str|None,
        niveau: str|None, 
        begint_met: str|None, 
        gedefinieerd: bool = False, 
        met_voorbeeld: bool = False,
        met_embed: bool = True,
        ):
        compiled_regex: re.Pattern|None = None
        if regex:
            try:
                compiled_regex = re.compile(regex)
            except re.error:
                await i9n.response.send_message("Invalid regex.", ephemeral=True)

        if niveau and niveau.upper() not in ["A1", "A2", "B1", "B2", "C1", "C2"]:
            await i9n.response.send_message("Dat ERK-niveau bestaat niet.", ephemeral=True)
            return

        choice_dict = self.dict
        if begint_met:
            choice_dict = [entry for entry in choice_dict if entry.lemma.startswith(begint_met.lower())]

        if heeft:
            choice_dict = [entry for entry in choice_dict if heeft.lower() in entry.lemma.lower()]

        if gedefinieerd:
            choice_dict = [entry for entry in choice_dict if entry.definitie != "/"]

        if met_voorbeeld:
            choice_dict = [entry for entry in choice_dict if entry.voorbeeld != "/"]

        if niveau:
            choice_dict = [entry for entry in choice_dict if entry.erk == niveau.upper()]

        if compiled_regex:
            choice_dict = [entry for entry in choice_dict if compiled_regex.fullmatch(entry.lemma) != None]

        random_entry = random.choice(choice_dict)
        if met_embed:
            embed = GetEmbed(random_entry)
            await i9n.response.send_message("Alsjeblieft!", embed=embed)
        else:
            await i9n.response.send_message(f"{random_entry.voorvoegsel} {random_entry.lemma}")

    @app_commands.command(name="watbetekent", description="Wat betekent het woord ...?")
    @app_commands.describe(woord="Het woord")
    @genUtils.catcherrors
    async def watbetekent(self, i9n: discord.Interaction, woord: str):
        await i9n.response.send_message(f"Ik zoek nu \"{woord}\" op.")
        woord = woord.lower().strip()
        found_entries = []
        for entry in self.dict:
            forms = get_clean_entries(entry.vormen)
            if woord == entry.lemma:
                found_entries.append(entry)
                continue

            if woord in forms:
                found_entries.append(entry)
                continue

        if len(found_entries) == 0:
            await i9n.followup.send(f"Ik heb niets gevonden voor \"{woord}\".")
            with open(UNFOUND_WORDS_FILE, "r") as f: 
                lines = [line.strip() for line in f.readlines()]
            if woord not in lines: lines.append(woord)
            with open(UNFOUND_WORDS_FILE, "w") as f: 
                f.write("\n".join(sorted(lines)))
            return

        test_embeds = []

        for i, entry in enumerate(found_entries):
            embed = GetEmbed(entry)

            if len(found_entries) > 1: 
                title = embed.title or ""
                title = title + f" ({i+1})"
                embed.title = title

            test_embeds.append(embed)

        final_embed = menus.MenuPages(genUtils.MultiPageEmbed(test_embeds, per_page = 1))

        ctx = await self.bot.get_context(i9n)
        await i9n.followup.send("Alsjeblieft!")
        await final_embed.start(ctx)

    @commands.command()
    @genUtils.catcherrors
    async def wbherladen(self, ctx: commands.Context):
        assert isinstance(ctx.author, discord.Member)
        if not genUtils.isStaff(ctx.author): return

        await ctx.send("Reloading...")
        await ctx.defer()
        await self.readdict()
        await ctx.send("Done")

async def setup(bot):
    await bot.add_cog(scraperCog(bot), guilds = [discord.Object(id = id_server)])