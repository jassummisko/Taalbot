import discord, \
    data.botResponses as botResponses, \
    utils.genUtils as genUtils, typing, datetime, re
from discord import Emoji, PollMedia, app_commands
from discord.ext import commands
from data.localdata import *
from modules.logger import logger
from data.spy import ALL_SPY_IDS

class teamCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self) -> None:
        print("Team Cog is ready")

        self.douane_channel = typing.cast(discord.TextChannel, self.bot.get_channel(id_douane_channel))
        self.log_channel = logger.getLogChannel()
        try: self.bugged_users = logger.loadBuggedUsers()
        except FileNotFoundError: 
            print("Cannot load wiretap list. Setting tracked users to none.") 
            self.bugged_users = []

    @commands.Cog.listener()
    @genUtils.catcherrors
    async def on_member_join(self, member: discord.Member):
        if str(member.id) in ALL_SPY_IDS:
            await logger.log(botResponses.SPY_FOUND(member.mention))
            await self.douane_channel.send(botResponses.SPY_FOUND(member.mention))
            await member.ban()
        else:
            print(f"{member.name} with id {member.id} is not a spy ^- ^.")
            
    @commands.Cog.listener()
    async def on_member_update(self, member_before: discord.Member, member_after: discord.Member):
        # CHECK ROLE
        ## If role taken
        if len(member_before.roles) < len(member_after.roles):
            new_role = next(
                role 
                for role in member_after.roles 
                if role not in member_before.roles
            )

            # Log leerkracht
            if new_role.id == id_leerkracht_role:
                # TODO: Put in botResponses
                await logger.log(f"{member_after.mention} heeft de leerkrachtrol gekregen.")

            # Log niveau
            if new_role.name.lower().startswith("niveau"):
                # Remove and change back to fate role

                # TODO: Put in botResponses
                await logger.log(f"📊 {member_after.mention} heeft nu de volgende niveaurol: **{new_role.mention}**.")


        ## If role removed
        elif len(member_before.roles) > len(member_after.roles):
            old_role = next(
                role 
                for role in member_before.roles 
                if role not in member_after.roles
            )

            # Log leerkracht
            if old_role.id == id_leerkracht_role:
                # TODO: Put in botResponses
                await logger.log(f"{member_after.mention} heeft de leerkrachtrol niet meer.")

            # Log niveau
            if old_role.name.lower().startswith("niveau"):
                # TODO: Put in botResponses
                await logger.log(f"📊 {member_after.mention} heeft de volgende niveaurol niet meer: **{old_role.mention}**.")

        # CHECK TIMEOUT
        is_timeout_occured = not member_before.is_timed_out() and member_after.is_timed_out()
        if not is_timeout_occured: return

        log = [log async for log in member_before.guild.audit_logs(limit=1, action=discord.AuditLogAction.member_update)][0]

        user = typing.cast(discord.Member, log.user)
        target = typing.cast(discord.Member, log.target)

        # TODO: Add amount of time in log.

        is_douanier = False
        for role in user.roles:
            if role.id == id_douanier_role:
                is_douanier = True
        if not is_douanier: return

        await logger.log(botResponses.DOUANE_TIMEOUT(user.mention, target.mention))
        await self.douane_channel.send(botResponses.DOUANE_TIMEOUT(user.mention, target.mention))

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        assert (user := payload.member), "User not found"
        assert (emoji := payload.emoji), "Emoji not found"
        assert (channel := self.bot.get_channel(payload.channel_id)), "Channel not found"
        assert isinstance(channel, discord.TextChannel) or \
            isinstance(channel, discord.Thread) or isinstance(channel, discord.VoiceChannel), "Channel is not a text channel or thread"
        assert (message := await channel.fetch_message(payload.message_id)), "Message not found"
        assert isinstance(message, discord.Message), "Message is not a message"

        is_douanier = False
        for role in user.roles:
            if role.id == id_douanier_role:
                is_douanier = True
        if not is_douanier: return

        if emoji.name != "🛃": return

        try: 
            assert isinstance(message.author, discord.Member)
            await message.author.timeout(datetime.timedelta(hours=1))
        except:
            print("User not found")

        msg = message.content
        if msg.strip() == "": msg = "GEEN TEKST"

        await logger.log(botResponses.DOUANE_TIMEOUT(user.mention, message.author.mention))
        await self.douane_channel.send(botResponses.DOUANE_TIMEOUT(user.mention, message.author.mention))

        files = []
        for file in [await f.to_file() for f in message.attachments]:
            file.filename = f"SPOILER_{file.filename}"
            files.append(file)

        await logger.getLogChannel().send(botResponses.DOUANE_DELETED(user.mention, message.content), files=files)
        await self.douane_channel.send(botResponses.DOUANE_DELETED(user.mention, message.content), files=files)
        await message.delete()

    @commands.Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        if message.author.bot: return
        member = typing.cast(discord.Member, message.author)
        attachments = [await a.to_file() for a in message.attachments]
        for file in attachments: file.filename = f"SPOILER_{file.filename}"
        content = message.content or "GEEN CONTENT"

        logged_message_header = f"**{member.name}** has deleted a message. \n"
        logged_message = logged_message_header

        if logger.isUserTrackable(member, self.bugged_users):
            if content:
                if len(content + logged_message) > (1950): content = "The message is too long to log."
                else: logged_message += f"The message had the following content: \n{content}"
            await self.log_channel.send(logged_message.strip(), files=attachments)
        # TODO: BETTER LOGGING
        #else:
            #if message.mentions:
                #logged_message += f"The message mentions the following users: {', '.join([user.mention for user in message.mentions])} \n"
            #if attachments:
                #logged_message += f"The message contained {len(attachments)} attachment(s)"
            #attachments = []

        #await self.log_channel.send(logged_message.strip(), files=attachments)

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if before.author.bot: return
        member = typing.cast(discord.Member, before.author)
        attachments = [await a.to_file() for a in before.attachments]
        for file in attachments: file.filename = f"SPOILER_{file.filename}"
        content = before.content or "GEEN CONTENT"

        logged_message_header = f"**{member.name}** has edited the following message: {after.jump_url}. \n"
        logged_message = logged_message_header

        if logger.isUserTrackable(member, self.bugged_users):
            if content:
                logged_message += f"The message had the following content: \n{content}"

            if len(logged_message) > 2000:
                logged_message = logged_message_header + "The previous message is too long to log."

            final_msg = await self.log_channel.send(logged_message.strip(), files=attachments)
            await final_msg.edit(suppress=True)
        # TODO: BETTER LOGGING
        #else:
            #if before.mentions:
                #logged_message += f"The message mentioned the following users: {', '.join([user.mention for user in before.mentions])} \n"
            #if attachments:
                #logged_message += f"The message contained {len(attachments)} attachment(s)"
            #attachments = []

        #if len(logged_message) > 2000:
            #logged_message = logged_message_header + "The previous message is too long to log."

        #final_msg = await self.log_channel.send(logged_message.strip(), files=attachments)
        #await final_msg.edit(suppress=True)


    @commands.command()
    async def ex_template(self, ctx: commands.Context):
        pass

    @app_commands.command(name="overzicht", description="(STAFF ONLY) Toont overzicht van #inbox.")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def overzicht(self, i9n: discord.Interaction):
        mail_channel = typing.cast(discord.ForumChannel, self.bot.get_channel(id_mail_channel))
        assert (active_emote := mail_channel.get_tag(ModMailRoles.Actief)), "Active emote not found"

        cond_active_threads = False

        message_buffer = "ACTIEVE THREADS: \n"
        for thread in mail_channel.threads:
            if active_emote in thread.applied_tags:
                message_buffer += thread.mention + "\n"
                cond_active_threads = True
        message_buffer = message_buffer.strip()

        if not cond_active_threads:
            await i9n.response.send_message("Geen actieve threads.")
            return

        all_messages = []
        
        buffer = ""
        for line in message_buffer.splitlines():
            if len(buffer + line) > 2000:
                all_messages.append(buffer)
                buffer = ""
            else:
                buffer += line
        all_messages.append(buffer)

        await i9n.response.send_message(all_messages[0], ephemeral=True)
        if len(all_messages) < 2: return

        for message in all_messages[1:]:
            await i9n.followup.send(message, ephemeral=True)

    @app_commands.command(name="wiretap", description="(STAFF) Wiretap user")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def bug(self, i9n: discord.Interaction, lid: discord.Member):
        self.bugged_users.append(lid.id)
        logger.saveBuggedUsers(self.bugged_users)
        await i9n.response.send_message(f"{lid.mention} is being wiretapped.")

    @app_commands.command(name="untap", description="(STAFF) Untap user")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def unbug(self, i9n: discord.Interaction, lid: discord.Member):
        self.bugged_users.remove(lid.id)
        logger.saveBuggedUsers(self.bugged_users)
        await i9n.response.send_message(f"{lid.mention} is no longer being wiretapped.")

    @app_commands.command(name="klaar", description="(STAFF) Mark forum post as done")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def klaar(self, i9n: discord.Interaction):
        try:
            assert isinstance(i9n.channel, discord.Thread), "Not in thread"
            assert isinstance(i9n.channel.parent, discord.ForumChannel), "Not in forum"
            assert (i9n.channel.parent.id == id_mail_channel), "Not in inbox"
        except Exception as e:
            await i9n.response.send_message(e.args[0], ephemeral=True)
            return

        assert (active_tag := i9n.channel.parent.get_tag(ModMailRoles.Actief))
        assert (klaar_tag := i9n.channel.parent.get_tag(ModMailRoles.Klaar))
        await i9n.channel.remove_tags(active_tag)
        await i9n.channel.add_tags(klaar_tag)

        await i9n.response.send_message("Klaar!")

    @app_commands.command(name="pauze", description="(STAFF) Mark forum post as paused")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def pauze(self, i9n: discord.Interaction):
        try:
            assert isinstance(i9n.channel, discord.Thread), "Not in thread"
            assert isinstance(i9n.channel.parent, discord.ForumChannel), "Not in forum"
            assert (i9n.channel.parent.id == id_mail_channel), "Not in inbox"
        except Exception as e:
            await i9n.response.send_message(e.args[0], ephemeral=True)
            return

        assert (active_tag := i9n.channel.parent.get_tag(ModMailRoles.Actief))
        assert (pauze_tag := i9n.channel.parent.get_tag(ModMailRoles.Pauze))
        await i9n.channel.remove_tags(active_tag)
        await i9n.channel.add_tags(pauze_tag)

        await i9n.response.send_message("Post gepauseerd.")

    @app_commands.command(name="pause", description="(STAFF) Alias for /pauze")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def pause(self, i9n: discord.Interaction):
        self.pauze(self, i9n)

    @app_commands.command(name="halt", description="(DOUANIER + STAFF) Gives a time out and logs it")
    @app_commands.default_permissions(moderate_members=True)
    @genUtils.catcherrors
    async def halt(self, i9n: discord.Interaction, lid: discord.Member):
        await lid.timeout(datetime.timedelta(hours=1))
        await logger.log(botResponses.DOUANE_TIMEOUT(i9n.user.mention, lid.mention))
        await self.douane_channel.send(botResponses.DOUANE_TIMEOUT(i9n.user.mention, lid.mention))
        await i9n.response.send_message(f"Timeout gegeven aan {lid.mention}.", ephemeral=True)

    @app_commands.command(name="unhalt", description="(DOUANIER + STAFF) Removes timeout from user and logs it")
    @app_commands.default_permissions(moderate_members=True)
    @genUtils.catcherrors
    async def unhalt(self, i9n: discord.Interaction, lid: discord.Member):
        await lid.timeout(None)
        await logger.log(botResponses.DOUANE_UNTIMEOUT(i9n.user.mention, lid.mention))
        await self.douane_channel.send(botResponses.DOUANE_UNTIMEOUT(i9n.user.mention, lid.mention))
        await i9n.response.send_message(f"Timeout verwijderd van {lid.mention}.", ephemeral=True)

    @app_commands.command(name="nieuwethread", description="Maak nieuwe thread met user")
    @app_commands.default_permissions(manage_messages=True)
    @genUtils.catcherrors
    async def nieuwethread(self, i9n: discord.Interaction, lid: discord.Member, titel: str):
        mail_channel = self.bot.get_channel(id_mail_channel)
        assert isinstance(mail_channel, discord.ForumChannel)
        thread = await mail_channel.create_thread(name=titel, content=lid.mention)
        assert(active_tag := mail_channel.get_tag(ModMailRoles.Actief)), "Active tag not found"
        assert(out_tag := mail_channel.get_tag(ModMailRoles.Out)), "Outbox tag not found"
        await thread.thread.add_tags(out_tag, active_tag)
        await thread.thread.send(botResponses.MAIL_REPLY_WITH_SENDMESSAGE())

        await i9n.response.send_message(f"Thread aangemaakt voor {lid.mention}.\nLink: {thread.thread.mention}", ephemeral=True)

async def setup(bot):
    await bot.add_cog(teamCog(bot), guilds = [discord.Object(id = id_server)])
