import discord
import math
from discord.ext import commands
from utils import genUtils
from modules.modmail.mailtypes import *
from modules.logger import logger
from data import botResponses
from data.localdata import *

async def sendMessage(bot: discord.Client, ctx: commands.Context):
    def getRecipientFromStarterEmbed(ctx: commands.Context, msg: discord.Message) -> discord.User | discord.Member:
        assert ctx.guild
        recipient_mention = msg.embeds[0].footer.text
        assert recipient_mention
        recipient = genUtils.memberByName(ctx.guild, recipient_mention)
        assert recipient

        return recipient

    def getRecipientFromStarterMessage(ctx: commands.Context, msg: discord.Message) -> discord.User | discord.Member:
        member_mention = starter_message.content.strip().split()[0]
        member_id = genUtils.mentionToId(member_mention)
        if not member_id: raise commands.CommandError(f"Cannot find user {member_mention}.")
        recipient = bot.get_user(member_id)
        assert recipient

        return recipient

    assert ctx.guild
    assert isinstance(ctx.author, discord.Member)
    if not genUtils.isStaff(ctx.author): raise commands.CommandError(botResponses.NOT_STAFF_ERROR())  

    if not isinstance(ctx.channel, discord.Thread): raise commands.CommandError(botResponses.MAIL_NOT_IN_THREAD())
    if not isinstance(ctx.channel.parent, discord.ForumChannel): raise commands.CommandError(botResponses.MAIL_NOT_IN_FORUM())
    if not ctx.channel.parent.id == id_mail_channel: raise commands.CommandError(botResponses.MAIL_NOT_IN_MAIL_CHANNEL())
    if not ctx.message.reference: raise commands.CommandError(botResponses.MAIL_NO_REPLY_MESSAGE())

    recipient: discord.User | discord.Member
    recipient_mention: str
    starter_message = await ctx.channel.fetch_message(ctx.channel.id)
    assert isinstance(starter_message, discord.Message)
    try:
        recipient = getRecipientFromStarterEmbed(ctx, starter_message)
        recipient_mention = recipient.mention
    except:
        recipient = getRecipientFromStarterMessage(ctx, starter_message)
        recipient_mention = recipient.mention

    msg_reference = ctx.message.reference
    assert msg_reference.message_id
    mail_message = await ctx.fetch_message(msg_reference.message_id)

    staff_member_count: int = 0
    for role in ctx.guild.roles:
        if role.id == StaffRoles.Staff:
            staff_member_count = len(role.members)

    needed_staff_count = math.ceil(staff_member_count/3)
    footer = f"---\nReply to this embed to respond to this mod mail.\nTicket ID: {genUtils.encode(ctx.channel.id)}"
    for reaction in mail_message.reactions:
        if reaction.emoji == "📨" and reaction.count >= needed_staff_count:
            await recipient.send(embed=botResponses.MAIL_EMBED_RECEIVED(f"**Mail van {ctx.author.name}, namens de staff**.", mail_message.content, footer))
            await ctx.send(botResponses.MAIL_SENT(recipient_mention))
            break
    else:
        await ctx.send(botResponses.MAIL_NOT_APPROVED(needed_staff_count))


async def sendMailWizard(bot: discord.Client, msg: discord.Message):
    channel = msg.author.dm_channel
    assert channel
    step: int = 0

    author: str = ""
    mention_author: str = ""
    content: str = ""
    mailtype: MailType = MailType.OTHER
    attachments: list[discord.File] = []

    async def stepMailType():
        nonlocal msg, mailtype, step
        await msg.channel.send(
            botResponses.MAIL_CHOOSE_MAILTYPE() \
            + "\n" + \
            botResponses.MAIL_ANON_NOTIF_AND_CANCEL()
        )

        def mailTypeCheck(m: discord.Message) -> bool:
            if m.channel.id != channel.id: return False
            if m.content.strip().lower() in m.content in ["oeps", "cancel"]: return True
            if m.content.isnumeric():
                selection = int(m.content)
                if selection in [e.value for e in MailType]:
                    return True
            return False

        msg = await bot.wait_for("message", check=mailTypeCheck, timeout=30)

        match msg.content.strip():
            case "oeps":
                step -= 2
                return
            case "cancel":
                step = -2
                return

        mailtype = MailType(int(msg.content))


    async def stepDraft():
        nonlocal attachments, content, step
        await msg.channel.send(
            botResponses.MAIL_TYPE_YOUR_MESSAGE() \
            + "\n" + \
            botResponses.MAIL_TYPE_TO_GO_BACK()
        )
        def messageCheck(m: discord.Message) -> bool: return m.channel.id == channel.id and m.author == msg.author

        finalmsg: discord.Message = await bot.wait_for("message", check=messageCheck, timeout=30*60)
        match finalmsg.content.strip():
            case "oeps":
                step -= 2
                return
            case "cancel":
                step = -2
                return
        attachments = [await attachment.to_file() for attachment in finalmsg.attachments]
        content = finalmsg.content

    async def stepWaitForSend():
        nonlocal msg, step, author, mention_author
        await msg.channel.send(
            botResponses.MAIL_TYPE_SEND() \
            + "\n" + \
            botResponses.MAIL_TYPE_TO_GO_BACK()
        )
        def sendCheck(m: discord.Message) -> bool:
            if m.channel.id != channel.id: return False
            if m.content.strip().lower() in ["oeps", "cancel", "send", "sendanon"]: return True
            return False

        sendmsg = await bot.wait_for("message", check=sendCheck)

        match sendmsg.content.strip().lower():
            case "oeps":
                step -= 2
                return
            case "cancel":
                step = -2
                return
            case "sendanon":
                author = "/"
                mention_author = "Anonymous"
            case "send":
                author = msg.author.name
                mention_author = msg.author.name

    all_steps = [
        stepMailType,
        stepDraft,
        stepWaitForSend,
    ]

    while step < len(all_steps):
        await all_steps[step]()
        step += 1
        if step == -1: 
            await msg.channel.send("Message cancelled.")
            return

    # Send message
    mail = newMail(content, mailtype, author)

    addNewMailToInbox(mail)

    mail_channel = bot.get_channel(id_mail_channel)
    assert isinstance(mail_channel, discord.channel.ForumChannel)

    mail_title = f"{mailtype.name.title()}"
    if author == "":
        author = msg.author.name
        await logger.log(f"Attempted to repair modmail name. User: {author}")
    if author != "/": mail_title += f" by {author}"
    
    thread_w_message = await mail_channel.create_thread(
        embed=botResponses.MAIL_EMBED_RECEIVED("Mail received", mail.message, mention_author), 
        name=mail_title,
        files=attachments
    )
    
    if author != "/": await thread_w_message.thread.send(botResponses.MAIL_RESPOND())

    def _getTag(enum: ModMailRoles) -> discord.ForumTag:
        tag = mail_channel.get_tag(enum)
        assert isinstance(tag, discord.ForumTag)
        return tag

    tags: list[discord.ForumTag] = []

    assert (tag_active := mail_channel.get_tag(ModMailRoles.Actief)), "Tag not found"

    tags.append(_getTag(ModMailRoles.In))
    tags.append(tag_active)

    match mailtype:
        case MailType.OTHER:
            pass
        case MailType.FEEDBACK:
            tags.append(_getTag(ModMailRoles.Server))
        case MailType.REPORT:
            tags.append(_getTag(ModMailRoles.Report))
        case MailType.NIVEAU:
            tags.append(_getTag(ModMailRoles.Niveau))
        case MailType.SESSIE:
            tags.append(_getTag(ModMailRoles.Server))
        case MailType.BOT:
            tags.append(_getTag(ModMailRoles.Server))

    await thread_w_message.thread.add_tags(*tags)

    await msg.channel.send(botResponses.MAIL_SENT("the staff team"))
