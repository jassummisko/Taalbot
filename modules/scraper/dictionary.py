from dataclasses import dataclass
import discord

WOORDSOORT_DICT = {
    "afk": "afkorting",
    "bnw": "bijvoeglijk naamwoord (adjectief)",
    "bw":  "bijwoord (adverbium)",
    "nvw": "nevenschikkend voegwoord",
    "ovw": "onderschikkend voegwoord",
    "tsw": "tussenwerpsel (interjectie)",
    "tw":  "telwoord (numerale)",
    "ww":  "werkwoord (verbum)",
    "vnw": "voornaamwoord (pronomen)",
    "vz":  "voorzetsel (prepositie)",
    "znw": "zelfstandig naamwoord (substantief)",
}

@dataclass
class DictEntry:
    voorvoegsel: str
    lemma: str
    ipa: str
    woordsoort: str
    vormen: str
    erk: str
    definitie: str
    voorbeeld: str

def CreateEntry(d) -> DictEntry: #d is a raw entry
    def getIndexOrSlash(tab: list, idx: int) -> str:
        try: 
            ret = tab[idx]
            if ret == "": ret = "/"
        except IndexError: 
            ret = "/"

        return str(ret).strip()

    voorvoegsel = getIndexOrSlash(d, 0)
    lemma = getIndexOrSlash(d, 1).replace("*", "")
    ipa = getIndexOrSlash(d, 2).replace("*", "")
    woordsoort = getIndexOrSlash(d, 3).replace("*", "")
    vormen = getIndexOrSlash(d, 4).replace("*", "")
    erk = getIndexOrSlash(d, 5).replace("*", "")
    definitie = getIndexOrSlash(d, 6).replace("*", "")
    voorbeeld = getIndexOrSlash(d, 7).replace("*", "")

    return DictEntry(
        voorvoegsel,
        lemma,
        ipa,
        woordsoort,
        vormen,
        erk,
        definitie,
        voorbeeld,
    )

def GetEmbed(de: DictEntry) -> discord.Embed:
    embed: discord.Embed

    woordsoort = WOORDSOORT_DICT.get(de.woordsoort) or "?"

    voorvoegsel = ""
    if de.voorvoegsel.lower() != "/":
        voorvoegsel = de.voorvoegsel

    full_name = f"{voorvoegsel} {de.lemma}".strip()

    title = f"Lemma: {full_name}"
    
    description = f"[{de.ipa}]\n"
    description += f"__Vormen:__ {de.vormen.replace('*', '')}\n"
    description += f"__ERK-niveau:__ {de.erk}\n"
    description += f"__Definitie:__ {de.definitie}\n"
    description += f"__Voorbeeldzin:__ {de.voorbeeld}\n"
    description = description.strip()

    footer = woordsoort

    embed = discord.Embed(title=title,description=description).set_footer(text=footer)

    return embed
