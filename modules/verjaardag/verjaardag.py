from datetime import date
import json

CAL_PATH = "modules/verjaardag/cal.json"

BdayCalendar = dict[int, str]

def loadCal() -> BdayCalendar:
    with open(CAL_PATH, "r") as f:
        raw_cal = json.load(f)
    final_cal = {int(k): v for k, v in raw_cal.items()}
    return final_cal

def saveCal(entries: BdayCalendar):
    with open(CAL_PATH, "w") as f:
        f.write(json.dumps(entries, indent=2))

def isValidDate(day: int, month: int) -> bool:
    try:
        x = date(1, month, day)
        return True
    except ValueError:
        return False

def isEntryMatchingDate(entry: str, date: date) -> bool:
    split_entry = entry.split("/")
    day, month = int(split_entry[0]), int(split_entry[1])
    return (day == date.day) and (month == date.month)

def getBirthdayIdsToday(cal: BdayCalendar) -> list[int]:
    today = date.today()
    return [usr_id for usr_id, entry in cal.items() if isEntryMatchingDate(entry, today)]

if __name__ == "__main__":
    pass