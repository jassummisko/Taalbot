from data import botResponses
from data.localdata import id_log_channel, id_welcome_channel, id_warning_role, id_tracker_role
from utils import genUtils
import discord, datetime
from taalbot import BOT

BUGGED_USERS_FILENAME = "bugged_users.txt"

async def logVcActivity(member: discord.Member, m_before: discord.VoiceState, m_after: discord.VoiceState):
    log_channel = getLogChannel()

    name_to_report = member.mention

    if genUtils.isStaff(member):
        name_to_report = member.name

    #User joined
    if not m_before.channel and m_after.channel:
        await log_channel.send(
            botResponses.LOG_USER_JOINED_VC(
                name_to_report,
                m_after.channel.mention,
            )
        )
    #User left
    elif m_before.channel and not m_after.channel:
        await log_channel.send(
            botResponses.LOG_USER_LEFT_VC(
                name_to_report,
                m_before.channel.mention,
            )
        )
    #User switched VC
    elif m_before.channel and m_after.channel:
        if not (m_before.channel is m_after.channel):
            await log_channel.send(
                botResponses.LOG_USER_LEFT_VC(
                    name_to_report,
                    m_before.channel.mention,
                )
            )
            await log_channel.send(
                botResponses.LOG_USER_JOINED_VC(
                    name_to_report,
                    m_after.channel.mention,
                )
            )

async def log(string: str):
    log_channel = getLogChannel()

    await log_channel.send(string)

def getLogChannel() -> discord.TextChannel:
    log_channel = BOT.get_channel(id_log_channel)
    assert isinstance(log_channel, discord.TextChannel), \
        Exception(f"Text channel with ID {id_log_channel} not found")
    return log_channel

def getWelcomeChannel() -> discord.TextChannel:
    welcome_channel = BOT.get_channel(id_welcome_channel)
    assert isinstance(welcome_channel, discord.TextChannel), \
        Exception(f"Text channel with ID {id_welcome_channel} not found")
    return welcome_channel

def saveBuggedUsers(user_id_list: list[int]):
    with open(BUGGED_USERS_FILENAME, "w") as file:
        file.writelines([str(id) for id in user_id_list])

def loadBuggedUsers() -> list[int]: 
    user_ids: list[int] = []
    with open(BUGGED_USERS_FILENAME, "r") as file:
        for line in file.readlines():
            if line: user_ids.append(int(line))

    return user_ids

def isUserTrackable(member: discord.Member, trackable_ids: list[int]):
        assert (joindate := member.joined_at)
        joindate = joindate.replace(tzinfo=None)
        one_day = datetime.timedelta(days=1)
        today = datetime.datetime.today().replace(tzinfo=None)

        is_new_user = today - joindate < one_day
        is_bugged = member.id in trackable_ids
        is_tracked_or_warned = False
        for role in member.roles:
            if role.id == id_warning_role or role.id == id_tracker_role: 
                is_tracked_or_warned = True
                break

        return is_new_user or is_bugged or is_tracked_or_warned
